use percent_encoding::{utf8_percent_encode, AsciiSet, CONTROLS};
use reqwest::blocking::Client;
use reqwest::Url;

use std::collections::HashMap;

const FRAGMENT: &AsciiSet = &CONTROLS.add(b' ').add(b'"').add(b'<').add(b'>').add(b'`');

pub struct MatrixClient<'a> {
    http_client: &'a mut Client,
    homeserver_uri: String,
    token: String,
}
impl<'a> MatrixClient<'a> {
    pub fn new(client: &'a mut Client, homeserver_uri: &str, token: &str) -> Self {
        Self {
            http_client: client,
            homeserver_uri: homeserver_uri.to_string(),
            token: token.to_string(),
        }
    }

    pub fn get(
        &self,
        path: &str,
        params: &HashMap<&str, String>,
    ) -> Result<reqwest::blocking::Response, reqwest::Error> {
        let mut path = self.homeserver_uri.clone() + path;

        let mut first = true;
        for (k, v) in params {
            path += &format!(
                "{}{}={}",
                if first { "?" } else { "&" },
                utf8_percent_encode(k.as_ref(), FRAGMENT),
                utf8_percent_encode(v.as_ref(), FRAGMENT)
            );
            first = false;
        }

        self.http_client
            .get(Url::parse(&path).unwrap())
            .header("Authorization", format!("Bearer {}", self.token))
            .send()
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Filter {
    pub account_data: EventFilter,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub event_fields: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub event_format: Option<String>,
    pub presence: EventFilter,
    pub room: RoomFilter,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct EventFilter {
    pub limit: i32,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub types: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub not_types: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub senders: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub not_senders: Option<Vec<String>>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct RoomFilter {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rooms: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub not_rooms: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub include_leave: Option<bool>,
    pub account_data: RoomEventFilter,
    pub ephemeral: RoomEventFilter,
    pub state: RoomEventFilter,
    pub timeline: RoomEventFilter,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct RoomEventFilter {
    pub limit: i32,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rooms: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub types: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub not_types: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub not_rooms: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub senders: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub not_senders: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub contains_url: Option<bool>,
}

impl Filter {
    pub fn new() -> Self {
        Self {
            account_data: EventFilter {
                limit: 0,
                types: None,
                not_types: None,
                senders: None,
                not_senders: None,
            },
            event_fields: None,
            event_format: None,
            presence: EventFilter {
                limit: 0,
                types: None,
                not_types: None,
                senders: None,
                not_senders: None,
            },
            room: RoomFilter {
                rooms: None,
                not_rooms: None,
                include_leave: None,
                account_data: RoomEventFilter {
                    limit: 0,
                    rooms: None,
                    types: None,
                    not_types: None,
                    not_rooms: None,
                    senders: None,
                    not_senders: None,
                    contains_url: None,
                },
                ephemeral: RoomEventFilter {
                    limit: 0,
                    rooms: None,
                    types: None,
                    not_types: None,
                    not_rooms: None,
                    senders: None,
                    not_senders: None,
                    contains_url: None,
                },
                state: RoomEventFilter {
                    limit: 0,
                    rooms: None,
                    types: None,
                    not_types: None,
                    not_rooms: None,
                    senders: None,
                    not_senders: None,
                    contains_url: None,
                },
                timeline: RoomEventFilter {
                    limit: 0,
                    rooms: None,
                    types: None,
                    not_types: None,
                    not_rooms: None,
                    senders: None,
                    not_senders: None,
                    contains_url: None,
                },
            },
        }
    }
}
