extern crate getopts;

#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate serde_yaml;

extern crate percent_encoding;
extern crate reqwest;

extern crate chrono;

mod matrix;
use matrix::*;

use getopts::Options;
use std::env;

use reqwest::blocking::Client;

use chrono::prelude::*;

use std::collections::HashMap;
use std::fs::File;
use std::fs::{create_dir_all, remove_file, write, OpenOptions};
use std::io::prelude::*;
use std::path::Path;

#[cfg(any(unix))]
use std::os::unix::fs::symlink;
#[cfg(any(windows))]
use std::os::windows::fs::symlink_file;

fn escape_file_name(name: &str) -> String {
    #[cfg(any(unix))]
    return name.replace("/", "-").replace("\\", "-");

    #[cfg(any(windows))]
    return name
        .replace("*", "-")
        .replace("\\", "-")
        .replace("/", "-")
        .replace(":", ".")
        .replace("*", "-")
        .replace("?", ".")
        .replace("\"", "-")
        .replace("<", "-")
        .replace(">", "-")
        .replace("|", "-");
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let mut opts = Options::new();
    opts.optopt(
        "c",
        "config",
        "Path to config.yaml. Default: ./config.yaml",
        "config.yaml",
    );
    opts.optflag("h", "help", "print this help menu");

    let print_usage = || {
        let brief = format!("Usage: {} [options]", args[0]);
        print!("{}", opts.usage(&brief));
    };

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => panic!(f.to_string()),
    };
    if matches.opt_present("h") {
        print_usage();
        return;
    }
    let config_path = matches.opt_str("c").unwrap_or("./config.yaml".to_owned());

    // Config file parsing
    #[derive(Debug, PartialEq, Serialize, Deserialize)]
    struct Config {
        homeserver: String,
        token: String,
        filter: ConfigFilter,
        logs_path: String,
    }
    #[derive(Debug, PartialEq, Serialize, Deserialize)]
    struct ConfigFilter {
        #[serde(rename = "type")]
        _type: String,
        list: Vec<String>,
    }
    let mut config_file = File::open(config_path).expect("Could not open configuration file");
    let mut config_content = String::new();
    config_file
        .read_to_string(&mut config_content)
        .expect("Could not read configuration file");

    let config: Config =
        serde_yaml::from_str(&config_content).expect("Invalid config file content");

    // Folder creation
    if !Path::new(&config.logs_path).is_dir() {
        panic!(
            "Log folder {:?} does not exist / is not a directory",
            &config.logs_path
        );
    }

    for name in ["id", "name", "canonical_alias"].iter() {
        let path = Path::new(&config.logs_path).join(name);
        create_dir_all(Path::new(&config.logs_path).join(name))
            .expect(&("Cannot create directory: ".to_owned() + path.to_str().unwrap()));
    }

    // State file parsing
    #[derive(Debug, PartialEq, Serialize, Deserialize)]
    struct State {
        since: String,
        rooms: HashMap<String, StateRoom>,
    }
    #[derive(Debug, PartialEq, Serialize, Deserialize)]
    struct StateRoom {
        name: String,
        canonical_alias: String,
    }
    let mut state: State;

    if let Ok(mut file) = File::open(Path::new(&config.logs_path).join("state.yaml")) {
        let mut state_content = String::new();
        file.read_to_string(&mut state_content)
            .expect("Could not read state file content");

        state = serde_yaml::from_str(&state_content).expect("Invalid state file");
    } else {
        state = State {
            since: String::new(),
            rooms: HashMap::new(),
        };
    }

    fn update_room_state_link(
        config: &Config,
        state: &mut State,
        room_id: &str,
        state_type: &str,
        state_new_value: &str,
    ) {
        assert!(state_type == "name" || state_type == "canonical_alias");

        // Remove previous link
        if let Some(ref room) = state.rooms.get(room_id) {
            let state_old_value = match state_type {
                "name" => &room.name,
                "canonical_alias" => &room.canonical_alias,
                _ => panic!(),
            };

            if state_old_value != "" {
                if state_new_value == state_old_value {
                    return;
                }
                // Remove old link
                let old_path = Path::new(&config.logs_path)
                    .join(state_type)
                    .join(&(escape_file_name(state_old_value) + ".log"));
                if let Err(e) = remove_file(&old_path) {
                    eprintln!(
                        "Warning: cannot remove old room name symlink {}: {}",
                        old_path.to_str().unwrap(),
                        e
                    );
                }
            }
        }

        // Insert StateRoom for room_id if none
        if let None = state.rooms.get(room_id) {
            state.rooms.insert(
                room_id.to_string(),
                StateRoom {
                    name: String::new(),
                    canonical_alias: String::new(),
                },
            );
        }

        // New link paths
        let target_path = Path::new("..")
            .join("id")
            .join(escape_file_name(room_id) + ".log");
        let file_path = Path::new(&config.logs_path)
            .join(state_type)
            .join(&(escape_file_name(state_new_value) + ".log"));

        // Create new link
        if file_path.is_file() {
            eprintln!(
                "Warning: Symlink {} already exists",
                file_path.to_str().unwrap()
            );
        } else {
            #[cfg(any(unix))]
            symlink(target_path, file_path).expect("Could not create symlink");

            #[cfg(any(windows))]
            symlink_file(target_path, file_path).expect("Could not create symlink");

            let room_state = state.rooms.entry(room_id.to_string()).or_insert(StateRoom {
                name: String::new(),
                canonical_alias: String::new(),
            });
            match state_type {
                "name" => room_state.name = state_new_value.to_string(),
                "canonical_alias" => room_state.canonical_alias = state_new_value.to_string(),
                _ => panic!(),
            };
        }
    };

    // Create matrix client
    let mut http_client = Client::new();
    let client = MatrixClient::new(&mut http_client, &config.homeserver, &config.token);

    // Sync response structs
    #[derive(Debug, PartialEq, Deserialize)]
    struct SyncRes {
        rooms: SyncResRooms,
        next_batch: String,
    }
    #[derive(Debug, PartialEq, Deserialize)]
    struct SyncResRooms {
        join: HashMap<String, SyncResRoom>,
    }
    #[derive(Debug, PartialEq, Deserialize)]
    struct SyncResRoom {
        state: SyncResState,
        timeline: SyncResTimeline,
    }
    #[derive(Debug, PartialEq, Deserialize)]
    struct SyncResTimeline {
        events: Vec<SyncResEvent>,
        limited: bool,
    }
    #[derive(Debug, PartialEq, Deserialize)]
    struct SyncResState {
        events: Vec<SyncResEvent>,
    }
    #[derive(Debug, PartialEq, Deserialize, Clone)]
    struct SyncResEvent {
        event_id: String,
        #[serde(rename = "type")]
        _type: String,
        sender: String,
        origin_server_ts: u64,
        prev_content: Option<serde_json::Value>,
        unsigned: Option<serde_json::Value>,
        content: serde_json::Value,
        state_key: Option<String>,
    }

    // Sync URI parameter list
    let mut params: HashMap<&str, String> = HashMap::new();
    params.insert("set_presence", "offline".to_owned());
    params.insert("timeout", "60000".to_owned());
    let mut filter = Filter::new();
    match config.filter._type.as_str() {
        "whitelist" => filter.room.rooms = Some(config.filter.list.clone()),
        "blacklist" => filter.room.not_rooms = Some(config.filter.list.clone()),
        _ => panic!(
            "Unknown filter type {:?}. Check your config.yaml.",
            config.filter._type
        ),
    }
    filter.room.state.limit = 100;
    filter.room.state.types = Some(vec![
        "m.room.name".to_owned(),
        "m.room.canonical_alias".to_owned(),
    ]);
    filter.room.timeline.limit = 100;
    filter.room.timeline.types = Some(vec![
        "m.room.member".to_owned(),
        "m.room.message".to_owned(),
        "m.room.name".to_owned(),
        "m.room.canonical_alias".to_owned(),
    ]);
    let filter_json = serde_json::to_string(&filter).unwrap();
    params.insert("filter", filter_json);

    loop {
        // Set since
        if !state.since.is_empty() {
            *params.entry("since").or_insert(String::new()) = state.since.clone();
        }

        // Do a sync !
        let sync = client.get("/_matrix/client/r0/sync", &params);
        if let Err(err) = &sync {
            // err.kind is private. using err.description() is a hacky solution
            if !err.is_timeout() {
                if let Some(status) = err.status() {
                    eprintln!("HTTP Error status={}: {}", status, err);
                    std::thread::sleep(std::time::Duration::from_secs(10));
                } else {
                    eprintln!("HTTP Error {}", err);
                    std::thread::sleep(std::time::Duration::from_secs(1));
                }
            }
            continue;
        }

        // Process sync data
        let text_data = sync
            .unwrap()
            .text()
            .expect("Could not read Sync response content");
        let data = serde_json::from_str::<SyncRes>(&text_data);

        if let Err(err) = data {
            eprintln!(
                "Sync response {:?} is not valid JSON / not a Sync response: {}",
                text_data, err
            );
            std::thread::sleep(std::time::Duration::from_secs(1));
            continue;
        }
        let data = data.unwrap();

        for (room_id, ref room_data) in &data.rooms.join {
            // println!("Sync: {:?}", room_id);

            match config.filter._type.as_str() {
                "whitelist" => {
                    if !config.filter.list.contains(&room_id) {
                        continue;
                    }
                }
                "blacklist" => {
                    if config.filter.list.contains(&room_id) {
                        continue;
                    }
                }
                _ => panic!(),
            }

            let log_path = Path::new(&config.logs_path)
                .join("id")
                .join(&(escape_file_name(room_id) + ".log"));

            // The room is new
            if !log_path.is_file() {
                println!("New room: {}", room_id);

                // Create file
                File::create(&log_path).expect("Could not create log file");

                // Read initial room state
                for state_event in &data
                    .rooms
                    .join
                    .get(room_id)
                    .expect("No state data for room")
                    .state
                    .events
                {
                    match state_event._type.as_str() {
                        "m.room.name" => {
                            let new_name = state_event.content["name"]
                                .as_str()
                                .expect("m.room.name has no content.name");

                            if new_name != "" {
                                update_room_state_link(
                                    &config, &mut state, &room_id, "name", &new_name,
                                );
                            }
                        }
                        "m.room.canonical_alias" => {
                            println!("state_event.content={:?}", state_event.content["alias"]);
                            let new_alias = state_event.content["alias"]
                                .as_str()
                                .expect("m.room.canonical_alias has no content.alias");

                            if new_alias != "" {
                                update_room_state_link(
                                    &config,
                                    &mut state,
                                    &room_id,
                                    "canonical_alias",
                                    &new_alias,
                                );
                            }
                        }
                        _ => {}
                    }
                }
            }

            let event_to_string = |event: &SyncResEvent| -> Option<String> {
                fn format_date(ts: u64) -> String {
                    let dt = DateTime::<Utc>::from_utc(
                        NaiveDateTime::from_timestamp((ts / 1000) as i64, (ts % 1000) as u32),
                        Utc,
                    )
                    .with_timezone(&Local);

                    format!("{}", dt.format("%Y-%m-%d %H:%M:%S"))
                }

                match event._type.as_str() {
                    "m.room.member" => {
                        // Find prev_content json object
                        let prev_content;
                        if let Some(ref pc) = event.prev_content {
                            prev_content = Some(pc);
                        } else if let Some(ref unsigned) = event.unsigned {
                            if let Some(ref pc) = unsigned.get("prev_content") {
                                prev_content = Some(pc);
                            } else {
                                prev_content = None;
                            }
                        } else {
                            prev_content = None;
                        }

                        // Extract prev_content data
                        let prev_membership;
                        let prev_display_name;
                        if let Some(ref prev) = prev_content {
                            prev_membership = prev["membership"].as_str().unwrap_or("leave");
                            prev_display_name = prev["displayname"].as_str().unwrap_or("");
                        } else {
                            prev_membership = "leave";
                            prev_display_name = "";
                        }

                        // Extract event data
                        let membership = event.content["membership"].as_str()?;
                        let display_name = event.content["displayname"].as_str().unwrap_or("");

                        if prev_membership == "leave" && membership == "invite" {
                            // Invite
                            if let Some(ref state_key) = event.state_key {
                                Some(format!(
                                    "{} {:24} {} has invited {} to the room\n",
                                    format_date(event.origin_server_ts),
                                    "--?",
                                    event.sender,
                                    state_key,
                                ))
                            } else {
                                Option::None
                            }
                        } else if (prev_membership == "leave" || prev_membership == "invite")
                            && membership == "join"
                        {
                            // Join room
                            let nicedispname = if !display_name.is_empty() {
                                format!("({})", display_name)
                            } else {
                                String::new()
                            };

                            Some(format!(
                                "{} {:24} {} {} has joined the room\n",
                                format_date(event.origin_server_ts),
                                "-->",
                                event.sender,
                                nicedispname,
                            ))
                        } else if prev_membership == "join" && membership == "leave" {
                            // Left
                            let nicedispname = if !display_name.is_empty() {
                                format!("({})", display_name)
                            } else {
                                String::new()
                            };

                            Some(format!(
                                "{} {:24} {} {} has left the room\n",
                                format_date(event.origin_server_ts),
                                "<--",
                                event.sender,
                                nicedispname,
                            ))
                        } else if prev_membership == "join" && membership == "join" {
                            // Nick
                            let sender = event.sender.as_str();

                            let niceprevdispname = if !prev_display_name.is_empty() {
                                format!("({})", prev_display_name)
                            } else {
                                String::new()
                            };

                            let nicedispname = if !display_name.is_empty() {
                                &display_name
                            } else {
                                sender
                            };

                            Some(format!(
                                "{} {:24} {} {} is now known as {}\n",
                                format_date(event.origin_server_ts),
                                "--",
                                sender,
                                niceprevdispname,
                                nicedispname,
                            ))
                        } else {
                            Option::None
                        }
                    }
                    "m.room.message" => {
                        let msgtype;
                        if let Some(mt) = event.content.get("msgtype") {
                            msgtype = mt.as_str().unwrap_or("");
                        } else {
                            msgtype = "";
                        }

                        match msgtype {
                            "m.emote" => Some(format!(
                                "{} {:24} {} {}\n",
                                format_date(event.origin_server_ts),
                                " *",
                                event.sender,
                                event.content["body"].as_str()?,
                            )),
                            _ => Some(format!(
                                "{} {:24} {}\n",
                                format_date(event.origin_server_ts),
                                event.sender,
                                event.content["body"].as_str()?,
                            )),
                        }
                    }
                    "m.room.name" => {
                        let new_name = event.content.get("name")?.as_str()?;

                        if !new_name.is_empty() {
                            update_room_state_link(
                                &config, &mut state, &room_id, "name", &new_name,
                            );
                        }

                        Some(format!(
                            "{} {:24} Room name changed to {}\n",
                            format_date(event.origin_server_ts),
                            "--",
                            new_name,
                        ))
                    }
                    "m.room.canonical_alias" => {
                        let new_alias = event.content.get("alias")?.as_str()?;

                        if !new_alias.is_empty() {
                            update_room_state_link(
                                &config,
                                &mut state,
                                &room_id,
                                "canonical_alias",
                                &new_alias,
                            );
                        }

                        Some(format!(
                            "{} {:24} Room canonical alias changed to {}\n",
                            format_date(event.origin_server_ts),
                            "--",
                            new_alias,
                        ))
                    }
                    _ => Option::None,
                }
            };

            let mut evs = room_data.timeline.events.clone();
            evs.sort_by_key(|e| e.origin_server_ts);
            let to_append = room_data
                .timeline
                .events
                .iter()
                .map(event_to_string)
                .filter(|text| text.is_some())
                .fold(String::new(), |acc, text| acc + &text.unwrap());

            if to_append != "" {
                let mut log_file = OpenOptions::new()
                    .append(true)
                    .open(&log_path)
                    .expect("Could not append to log file");

                log_file
                    .write(to_append.as_bytes())
                    .expect("Could not write to log file");
            }
        }

        state.since = data.next_batch;

        // Save state
        let state_yaml = serde_yaml::to_string(&state).unwrap();
        write(
            Path::new(&config.logs_path).join("state.yaml"),
            state_yaml.as_bytes(),
        )
        .expect("Could not write state file");
    }
}
