# Matrix logger

Log discussions on the [Matrix](https://matrix.org) network, using a format very similar to Weechat logs.

# Build

```bash
# Dev build
cargo build

# Release build
cargo build --release
```

# Setup

```bash
cp config.example.yaml config.yaml
$EDITOR config.yaml
# Edit `homeserver` and `token` fields
./matrix-logger
```


# Limitations

- Does not support message redaction
- Does not support end-to-end encryption. Encrypted rooms will only display unencrypted messages / state events.
- Does not download files / images
- Some `m.room.message` events may not be logged correctly depending on the `msgtype` field
- Requires administrator privileges on Windows to create symbolic links